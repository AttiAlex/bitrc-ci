import React, { Component } from 'react'

/**
 * Represents a title.
 * @constructor
 * @param {string} text - The text of the title.
 */
class Title extends Component {

  render () {
    const {
      text,
    } = this.props

    return (
      <h1>{text}</h1>
    )
  }
}

export default Title
