import React, { Component } from 'react'

/**
 * Represents a title.
 * @constructor
 * @param {string} text - The text of the subtitle.
 */
class Subtitle extends Component {

  render () {
    const {
      text,
    } = this.props

    return (
      <h2>{text}</h2>
    )
  }
}

export default Subtitle
